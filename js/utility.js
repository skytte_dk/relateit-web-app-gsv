// NAV Functions
// takePicture
// clearImageBuffer
// submitForm

// https://www.ibm.com/developerworks/community/blogs/94e7fded-7162-445e-8ceb-97a2140866a9/entry/upload_a_picture_using_phonegap_on_android8?lang=en
// Generic Function for submitting forms

var imageBuffer = [];
var imageSubmitFormID ="";
var imageSubmitURL ="";
var targetControl = "";


// Form Submit Functions

function submitForm(formID,URL) {
		
	showMainPage();
	var i = 0;
	var fd = new FormData();
    
	// form files
	$('input[type=file]').each(function(el,file)
	{
		if(file.files.length>0)
		{
			fd.append("file_"+el,file.files[0]);
		}
	});
	
	
	// imageBuffer files
	for(var j = 0;j<imageBuffer.length;j++){
		imageBuffer[i+j].FileName = imageBuffer[i+j].name+".jpg";
        fd.append("file_"+(i+j), imageBuffer[i+j]);
    }
	// clear the images buffer on submit
   imageBuffer = [];
   imageSubmitURL = "";
   imageSubmitFormID = "";
		
	// remaining form data
	form = $("#"+formID +" :input");
	
	for(var i=0; i < form.length; i++)
	//for(var i=0; i < form.elements.length; i++)
	{
		var e = form[i];
		if(e.type=='radio') {
			var selectedValue = $("input[name='"+e.name+"']:checked").val();
			try {
				fd.append(e.name,selectedValue);	
			} catch(ex) {}
		}if(e.type=='checkbox')
			fd.append(e.name,e.checked);	
		else {
			if(e.type!='submit')
			  fd.append(e.name,e.value);
		}
			
	}	
    
   load(getRootURL()+ URL, onModuleLoaded,null,fd); 
   // n�r vi har loaded skal imageBuffer Cleares.
}

// Picture Functions

function clearImageBuffer() {
	// clear the images buffer on submit
   imageBuffer = [];
}

function takePicture(submitOnSuccessFormID,submitOnSuccessURL) {
	imageSubmitURL = submitOnSuccessURL;
	imageSubmitFormID = submitOnSuccessFormID;
	//Se https://github.com/amirudin/PhoneGapPluginDemo/blob/master/www/camera.html
	navigator.camera.getPicture(onGetPictureSuccess, onGetPictureFail, 
		{ 	quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType : Camera.PictureSourceType.CAMERA
		});
}

function onGetPictureSuccess(imageData) {
	showMainPage();
	var imageFile = b64toBlob(imageData,'image/jpg',512);
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    imageFile.lastModifiedDate = new Date();
    imageFile.name = Math.random().toString(36).substr(2, 20);
	imageBuffer.push(imageFile)
	if(imageSubmitURL) {
		submitForm(imageSubmitFormID,imageSubmitURL);
	}
}

function onGetPictureFail(message) {
	var n = str.indexOf("cancelled");
	if(n==-1)
      alert('Picture failed because: ' + message);
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

// PDF Viewer Scanner Functions

function openPDF(URL){
	try {
            //var ref = window.open(URL, '_blank', 'location=yes');
            window.open(URL, '_blank', 'location=yes,enableViewportScale=yes');
	}catch(err) 
	{
		alert(err.message);
	}
}

function openPDF2(URL){
	try {
            window.plugins.ChildBrowser.showWebPage(URL,{ showLocationBar: true });
	}catch(err) 
	{
		alert(err.message);
	}
	
}


// functionality for filterable list using remote date

function listItemClick(listItem,sendValueTo,sendNameTo) {
	$("#autocomplete").empty();
	$("#autocomplete-input").val('');
	$('[name="'+sendValueTo+'"]').val($(listItem).attr('data-value'));
	$('[name="'+sendNameTo+'"]').val($(listItem).attr('data-name'));
}

function listCreate(dataURL,sendValueTo,sendNameTo) {
	$( "#autocomplete" ).on( "filterablebeforefilter", function ( e, data ) {
		var $ul = $( this ),
			$input = $( data.input ),
			value = $input.val(),
			html = "";
		$ul.html( "" );
		if ( value && value.length > 2 ) {
			$ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
			$ul.listview( "refresh" );
			$.ajax({
			url: ip + dataURL,
				dataType: "json",
				crossDomain: true,
				data: {
					"filter": $input.val(),
					"username" : username,
					"password" : password,
					"token" : APIKEY,
					"language" : captions[currentLanguage].currentLanguageName
				}
			})
			.then( function ( response ) 
				{
				$.each( response.result, function ( i, val ) {
					html += "<li onClick='listItemClick(this,\""+sendValueTo+"\",\""+sendNameTo+"\")' data-value='"+val.value+"' data-name='"+val.name+"'>" + val.name + "</li>";
				});
				$ul.html( html );
				$ul.listview( "refresh" );
				$ul.trigger( "updatelayout");
				});
		}
	});
}

// Barcode Scanner Functions

var scanInProgress = false;

function scanBarcode(target) {
 targetControl = target;
 
 //if(!scanInProgress) {
 //  scanInProgress = true;
   cordova.plugins.barcodeScanner.scan(
      function (result) {
		onscanBarcodeSuccess(result.text,result.cancelled);
      }, 
      function (error) {
		  onscanBarcodeFail(error);
      }
   );
 // }
}

function onscanBarcodeSuccess(message,cancelled) {
	//scanInProgress = false;
	if(cancelled==false) {
	  $('#'+targetControl).val(message);
	  $('#'+targetControl).change();	  
	}  
	showMainPage();		
}

function onscanBarcodeFail(message) {
	//scanInProgress = false;
    //alert('Scan failed because: ' + message);
}



