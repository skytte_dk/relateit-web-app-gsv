﻿var APIKEY = "64e7c706fd6f90342c090a7d13ac08a4";
var mainUrl ='';
var hashString = '';
var currentLanguage = 'dk';
var username = '';
var password = '';
var domain = 'd';
var ip = '';
var port = '';
var rememberMe = false;
var hwid = "";
var deviceType = 0;
var notificationCount = 0;
var loadingText = '';
var currentRoute = '';
var currentModuleRoute = '';
var moduleMode = false;
var applicationActive = false;
var isLoading = false;
var systemRefresh = false;
var showDefaultSpinner = true;
var spinner;
var settings = {};
var captions = [];
var geoTag = {};
var serverList = [];
var serverIp = "";


function isMobileDevice() {
	return(navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/));
}
function setupLanguages() {

	captions['gb'] = {};
	captions['dk'] = {};
	
	captions['gb'].remember_me = 'Remember me';
	captions['dk'].remember_me = 'Husk mig';
	
	captions['gb'].yes = 'Yes';
	captions['dk'].yes = 'Ja';	
	
	captions['gb'].no = 'No';
	captions['dk'].no = 'Nej';	
	
	captions['gb'].username = 'Username';
	captions['dk'].username = 'Brugernavn';	
	
	captions['gb'].password = 'Password';
	captions['dk'].password = 'Kodeord';	
	
	captions['gb'].login = 'Login';
	captions['dk'].login = 'Log på';	
	
	captions['gb'].exit = 'Exit';
	captions['dk'].exit = 'Afslut';	
	
	captions['gb'].login_error = 'Login error! please check login credentials';
	captions['dk'].login_error = 'Login fejl! Tjek venligst login informationer';	
	
	captions['gb'].connection_error = 'Connection error!';
	captions['dk'].connection_error = 'Forbindelses fejl!';

	captions['gb'].settings = 'Login Menu';
	captions['dk'].settings = 'Login menu';

	captions['gb'].refresh = 'Refresh';
	captions['dk'].refresh = 'Opdatér';

	captions['gb'].name = 'Name';
	captions['dk'].name = 'Navn';	
	
	captions['gb'].title = 'Title';
	captions['dk'].title = 'Stilling';	
	
	captions['gb'].home = 'Home';
	captions['dk'].home = 'Hjem';

	captions['gb'].email = 'Email';
	captions['dk'].email = 'Email';	
	
	captions['gb'].loading = 'Loading';
	captions['dk'].loading = 'Indlæser';

	captions['gb'].currentLanguageName = 'ENG';	
	captions['dk'].currentLanguageName = 'DAN';

	captions['gb'].IP = 'Server';	
	captions['dk'].IP = 'Server';
}
function isLoggedIn() {
    return ($.mobile.activePage.attr("id") == "index");
}
    
function changeLanguage(code) {
	currentLanguage = code;
	switch(code.trim()) {
		case 'dk': $(".ui-slider-track .ui-btn.ui-slider-handle").css("background-image","url('./css/icons/denmark_round_32.png')");
			break;
		case 'gb': $(".ui-slider-track .ui-btn.ui-slider-handle").css("background-image","url('./css/icons/united-kingdom_round_32.png')");
			break;
		default: $(".ui-slider-track .ui-btn.ui-slider-handle").css("background-image","url('./css/icons/denmark_round_32.png')");
	}
	
	
	$("#txtUsername").attr("placeholder",captions[currentLanguage].username);
	$("#txtPassword").attr("placeholder",captions[currentLanguage].password);
	$("#txtIP").attr("placeholder",captions[currentLanguage].IP);
	$("#btnLogin").text(captions[currentLanguage].login);
	$("#btnSettings").text(captions[currentLanguage].settings);
	$("#lblName").text(captions[currentLanguage].name);
	$("#lblTitle").text(captions[currentLanguage].title);
	$("#lblEmail").text(captions[currentLanguage].email);
	$("#lblLanguage").text(captions[currentLanguage].currentLanguageName);
    $("#txtRemember_me").text(captions[currentLanguage].remember_me);
}

function resetSettings() {
		username = '';
		password = '';
		currentLanguage = 'dk';
		
		rememberMe = false;
		window.localStorage.setItem('ip', ip);
		window.localStorage.setItem('username', username);
		window.localStorage.setItem('password', password); 
		window.localStorage.setItem('language', currentLanguage);
		window.localStorage.setItem('remember', rememberMe);
		loadSettings();
}

function getSetting(name)  {
    try {
        return window.localStorage.getItem(name);
    } catch (ex) {
        return '';
    }
}
	
function getServerList()  {
	if(getSetting('serverList')) 
	{
		serverList = JSON.parse(getSetting('serverList'));
		serverIp = getSetting('serverIp');
	} else {
		username = getSetting('username');
		password = getSetting('password');
		remember = getSetting('remember');
		ip = getSetting('ip');
		if(ip) {
			saveServer({ip: ip,username: username,password: password,remember : remember})	
		} 
		saveServerList(serverList);
	}
	
}
function saveServerList()  {
	window.localStorage.setItem('serverList', JSON.stringify(serverList));
	window.localStorage.setItem('serverIp', serverIp);	
}

function getServer(ip) {
	for(var i = 0; i< serverList.length;i++)
	{
		server = serverList[i];
		if(server.ip == ip)
		  return i;
	}
	return -1;
}

function saveServer(server) {
	i = getServer(server.ip);
	if(i>-1)
	{
		serverList[i] = server;
	}else
	{
		serverList.push(server);	
	}
	serverIp = server.ip;
	saveServerList();
}

function onselectServerChanged() {
	window.localStorage.setItem('serverIp', this.value);	
	loadSettings();
}

function loadSettings() {
	
	    getServerList();
	    i = getServer(serverIp);
		//GSV>>
		/*
		var options = $("#selectServer");
		options.empty();
		$.each(serverList, function() {
			options.append(new Option(this.ip, this.ip));
		});
		options.val(serverIp);
		options.selectmenu("refresh");
		if (i>-1) {
			rememberMe = serverList[i].remember;
			ip =  serverList[i].ip;
		}  else
		{
			rememberMe = "0";
			ip = "";
		}
		*/
		rememberMe = getSetting('remember');
		//GSV<<
		
        currentLanguage = getSetting('language');
		
        if (!currentLanguage)
            resetSettings();
        if(currentLanguage=="gb") {
            $("#select_language")[0].selectedIndex = 1;
		}
        else {
			$("#select_language")[0].selectedIndex = 0;
			
		}

        $("#select_language").slider();
        $("#select_language").slider('refresh');
    


		
		if (rememberMe == "1") {
		    $("#select_remember").attr("checked",true).checkboxradio("refresh");
			//GSV>>
			//username =  serverList[i].username;
		    //password = serverList[i].password;
			ip =  getSetting('ip');
			username =  getSetting('username');
		    password = getSetting('password');
			//GSV<<
		} else 
		   $("#select_remember").attr("checked",false).checkboxradio("refresh");
		
		$('#txtIP').val(ip);
		$('#txtUsername').val(username);
		$('#txtPassword').val(password);
	}
	
function saveSettings()		{
    rememberMe = $("#select_remember")[0].checked;
	username =	$('#txtUsername').val(); 
	password = $('#txtPassword').val();
	ip =  $('#txtIP').val();
	
	if (rememberMe) {
	    window.localStorage.setItem('username', username);
	    window.localStorage.setItem('password', password);
		saveServer({ip: ip,username: username,password: password,remember : rememberMe})
	} else {
	    window.localStorage.setItem('username', '');
	    window.localStorage.setItem('password', '');
		saveServer({ip: ip,username: '',password: '',remember : rememberMe})
	}
	window.localStorage.setItem('language', currentLanguage);
	window.localStorage.setItem('ip', ip);
	
	if (rememberMe)
	  window.localStorage.setItem('remember', "1");
    else
	  window.localStorage.setItem('remember', "0");
}

function getRootURL() {
	//GSV>>
	//ip = 'https://nav.gsv.dk:8076';  
	realId ='';
	if(ip.toLowerCase()=="gsv") {
		realId = 'https://nav.gsv.dk:8076';
	} else if(ip.toLowerCase()=="test.gsv") {
		realId = 'https://navtest.gsv.dk:8077';
	}
	//GSV<<
	return realId;
}

function createModuleList(res,callBack) {
	// userinfo
	var preloadURL = '';
	var preloadContainer = '';
	
    var userHTML = '<li>';
    var jd = {};
    jd.username = '';
    jd.title = '';
    jd.email = '';
    jd.modules = {};

   
	showMainPage();		


		
	isLoading = true;
    try { jd = JSON.parse(res); } catch (ex) { }
	  
    $('#left_panel ul').empty();
    if(!moduleMode) 
	  $('#main_content').empty();

	//Add logo dynamically
	if (jd.logo === undefined || jd.logo === null) {
	var standardLogo = "background-image:url('./css/images/RelateIT_Logo_Symbol_dark.png')";
	
    $('.ui-header .ui-grid-b .ui-block-b').attr('style', standardLogo);

  } else {
  	//Custom Logo
  	var customLogo = "background-image:url('data:image/png;base64," + jd.logo + "');";
  	$('.ui-header .ui-grid-b .ui-block-b').attr('style', customLogo);
  }

     //Opbyg bruger info header
      userHTML += '<div class="ui-corner-all custom-corners" >';
	  userHTML += '<div class="ui-bar ui-bar-a">';
	  userHTML += '<table style="padding: 0;margin: 0;">';
	  userHTML += '<tr><td>' + jd.username + '</td></tr>';
	  userHTML += '<tr><td>' + jd.title + '</td></tr>';
	  userHTML += '<tr><td>' + jd.email + '</td></tr>';
	  userHTML += '<table></div></div>';
	  userHTML +='</li>';
	  
	  
	  // Add modules
	  $("#left_panel ul").append(userHTML);
	  $.each( jd.modules, function( key, module ) 
	  {
		 var container ='';
		 var content = '';
		 
		 if (module.location == "Swipe Menu" || module.location == "Both") 
	 	{
	    	    // 1, Create Swipe Menu Items
		    container = "#left_panel ul";
		    content = '<li id ="m_' + module.id + '" class="ui-menu-icon-'+module.icon+'"><a href="">' + module.description + '<span id ="c_' + module.id + '" class="ui-li-count">' + module.notifications.toString() + '</span></a></li>';
			$("#left_panel ul").append($(content).click(function() {
			    $("#left_panel").panel("close");
			    showMainPage(true);
				url = getRootURL() + module.route;
				moduleMode = true;
				currentModuleRoute = url;
				load(url, onModuleLoaded);
			}));
			
			if(module.default=="true")  {
				preloadURL = getRootURL() + module.route;
				preloadContainer =module.name+'_content';
			}

			if (module.notifications == 0)
			    $('#c_' + module.id).hide();

		 } else if (module.location == "Home Screen" || module.location == "Both") 
		{
           		if(!moduleMode) {
			    // 2. Create Main Items
				content = '<div id="m_'+module.id+'" data-role="collapsible" data-collapsed="'+module.collapsed+'" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right">' + 
							  '<h3>'+module.description+'</h3><p id="'+module.name+'_content"></p></div>';	
				container = "#main_content";
				if(module.collapsed=="false")  
				{
					url = getRootURL() + module.route;
					preloadContainer =module.name+'_content';
				}
				// If not collapsed, and not a systemrefresh, den expand modeule
				$(container).append($(content).collapsible({
				    expand: function (url) {
						url = getRootURL() + module.route;
						moduleMode = false;
						currentModuleRoute = url;
						load(url,onHomeMenuExpand,module.name+'_content');
					}
				}));
            }
			}
	  });
	  
	  $("#left_panel ul").append('<li id ="m_settings"><a href="javascript:showLoginPage();">' + captions[currentLanguage].settings + '</a></li>');
	  if(!isiOS()) {
		$("#left_panel ul").append('<li id ="m_exit"><a href="javascript:onExitAppClick()">' + captions[currentLanguage].exit + '</a></li>');
	  }
	  $("#left_panel ul").listview('refresh');
	  $("#left_panel ul").trigger('create');

      // Create notificationlist
	  notificationCount = jd.notifications.length;
	  updateNotificationCount();

	  $('#right_panel ul').empty();
	  //GSV>>
	  container = "#right_panel ul";
	  content = '<li style="background-color:rgb(246, 246, 246);">Tilbage</li>';
	  $("#right_panel ul").append($(content).click(
          function (event) {  $( "#right_panel" ).panel( "close" ); }
       ));
	  //GSV<<
	  $.each(jd.notifications, function (key, notification) {
	      container = "#right_panel ul";
	      content = '<li id = "' + notification.module_id + '" guid="' + notification.guid + '" route="' + notification.route + '"><a href="">' + notification.message + '</a></li>';
	      $("#right_panel ul").append($(content).click(
              function (event) { onNotificationItemClick(notification,this); }
          ));
	  });

	  $("#right_panel ul").listview('refresh');
	  $("#right_panel ul").trigger('create');
	  $(document).on("swipeleft swiperight", "#right_panel li", function (event) {
	      event.stopImmediatePropagation();
	      onNotificationItemSwipe(event, this);
	  });

	 systemRefresh = false;
	 isLoading = false; 
	 if (callBack) {
	     callBack();
	 } else if(preloadURL!="" && !moduleMode) 
	 {
	 	 load(preloadURL, onModuleLoaded);
 	 }
  
}

function showMainPage(reset) {
    applicationActive = true;
    if (reset) {
    }
	//GSV>>
	$("#gradient_background").hide();
	//GSV<<
    $.mobile.changePage("#index", {transition: 'flip' ,reverse : false});    
}

function showLoginPage() {
    applicationActive = false;
    $("#lblErrorLogin").hide();
	//GSV>>
	$("#gradient_background").show();
	//GSV<<
    $.mobile.changePage("#loginPage", {transition: 'flip', reverse : true, direction : "back"});
}

function pull_main() { 
	if(isLoading == false) {
      showDefaultSpinner = false;
      refresh();
    }
}

function refresh() {
	if(isLoading == false)
	{
		url = getRootURL() + '/system/get_modules';
		if(moduleMode) {
		   systemRefresh = true;
		   load(currentModuleRoute, onModuleLoaded);
		}
		else
		  load(url, createModuleList);
	}
}

function loadCurrentModuleRoute() {
  load(currentModuleRoute, onModuleLoaded);
}

function loadModule() {
  load(currentModuleRoute, onModuleLoaded);
}

function onRefreshClick() {
    $("#left_panel").panel("close");
    $("#right_panel").panel("close");
    refresh();
}

function onExitAppClick() {
	
	if (navigator.app) {
      navigator.app.exitApp();
    }  else if (navigator.device) {
       navigator.device.exitApp();
	}
	
}

function onHomeClick() {
    $("#left_panel").panel("close");
    $("#right_panel").panel("close");
    moduleMode = false;
    refresh();
}

function updateNotificationCount(module_id) {
    $('#lblNotifications').html(notificationCount.toString());
}

function onNotificationItemClick(notification,parent) {
    $("#right_panel").panel("close");
    var listitem = $(parent);
	if(notification.route!="") {
		url = getRootURL() + notification.route;
		load(url, onModuleLoaded, listitem);
	};
}

function onNotificationItemSwipe(event,parent) {
	var listitem = $( parent ),
    dir = event.type === "swipeleft" ? "left" : "right",
    transition = $.support.cssTransform3d ? dir : false;
	removeNotification(listitem);
}

function removeNotification(listitem) {
    url = getRootURL() + '/system/remove_notification?guid=' + listitem.attr("guid");
    load(url, removeNotificationListItem, listitem);
}

function removeNotificationListItem(res,listitem) {
    listitem.remove();
    notificationCount--;
    updateNotificationCount(listitem[0].id);
    hideSpinner();
}

function onHomeMenuExpand(res,element){
	$("#"+element).html(res).trigger('create'); 
	$( "button" ).each(function( index ) {      
		$(this).click(customClickHandler);
	});
}

// geolocation
function onPositionUpdate(position) {
	geoTag.latitude = position.coords.latitude;
	geoTag.longitude = position.coords.longitude;
}

function onModuleLoaded(res, listItem) {
	
    moduleMode = true;
    showMainPage(true);
    $("#main_content").empty(); 
	var mainHeight =  $("#main").height();
	var HeaderHeight = $("#index > div.ui-panel-wrapper > div.ui-header.ui-bar-inherit").height();
	$("#main_content").height(mainHeight-HeaderHeight);
    $("#main_content").html(res).trigger('create'); 
	
	// Set custom click handler on butttons
	$( "button" ).each(function( index ) {        
		$(this).click(customClickHandler);
	});
				
    
	// Set custom click handler on links
	$(".nav_link").each(function (index) {        
	    $(this).click(customClickHandler);
	});
	
	if(listItem)
	    removeNotification(listItem);

	//Reload menu and notifications
    url = getRootURL() + '/system/get_modules';
	showDefaultSpinner = false;
    load(url, createModuleList);
	
	// Show toast messages
	if($('.popupBasic').size()>0) {
	  $('.popupBasic').popup('open');
	  setTimeout(closePopUps, 2000);
	}	    
}

function closePopUps(popUp) {
	try {$('.popupBasic').remove();} catch(ex) {alert(ex.message)}
}

function customClickHandler(e) {
    // Execute code in code tag
    var code;
    code = $(this).attr('execute');
    try {
        if (code)
            eval(code);
        } catch (ex)   {
      }
	
    if ($(this).attr('value') != '#') 
	{
        url = getRootURL() + $(this).attr('value');
        if ($(this).attr('SystemRefresh')) {
            systemRefresh =true;
        }
        load(url, onModuleLoaded);
        e.preventDefault();
    }
}

function performLogin() {
    saveSettings();
	$("#lblErrorLogin").text('');
	$("#lblErrorLogin").hide();
	url = getRootURL() + '/system/isalive?token=' + hwid + '&device_type=' + deviceType.toString();
    load(url, onIsAlive);
}
  
function onIsAlive(res) {
    if (res.trim() == "1") {
		moduleMode = false;
        refresh();
    } else {
		hideSpinner();
		showLoginPage();
		$("#lblErrorLogin").text(captions[currentLanguage].login_error);
		$("#lblErrorLogin").show();
    }
}

function showSpinner() {
	if (showDefaultSpinner) {
        $.mobile.loading("show", {
            textVisible: true,
            theme: "d",
            html: ""
        });
    }
}

function hideSpinner() {
	$.mobile.loading("hide");	
}

var callStack = [];
var modeStack = [];

function pushStack(URL) {
  callStack.push(URL);	
  modeStack.push(moduleMode);	
}

function popStack() {
	if(callStack.length==1) {
		moduleMode = modeStack.pop();
		return(callStack[0]);        // cannot pop last element
	} else {
		moduleMode = modeStack[0];
	    return(callStack.pop()); 
	}  
}

function  onBackKeyDown(event){
	// ToDo: implement a stack
	// http://stackoverflow.com/questions/1590247/how-do-you-implement-a-stack-and-a-queue-in-javascript
	event.preventDefault();
}
		
function load(url, callBack, passThroughData,addFormData) {
	
  
  if (isLoading == false) 
	{

		try {
		try{loadSettings();}	catch(ex) {}
		currentRoute = url;
		showSpinner();
			
		// remove any remaining popups
		$('#main').xpull({ 'paused': true });
		if(addFormData)
			requestData = addFormData;
		else
			requestData = new FormData();	
		
		
		requestData.append("username",username);
		requestData.append("token",APIKEY);
		requestData.append("password",password);
		requestData.append("latitude",geoTag.latitude );
		requestData.append("longitude",geoTag.longitude );
		try {
				requestData.append("language",captions[currentLanguage].currentLanguageName);
			} catch(ex) 
			{
				requestData.append("language","DAN");
			}
		
		isLoading = true;
		} catch(ex) 
		{
			isLoading = false;
			hideSpinner();
			alert(ex)
		}
		
		$.ajax({
          url: url,
          type: 'POST',
          data: requestData,
	      contentType: false,
		  processData: false,
		  context: this,
	  	  success: function (result) {
			  isLoading = false;
			  try{resetXpull();}   catch(ex) {}
			  $.proxy(callBack, this)(result, passThroughData);
		  	  if(callBack !== createModuleList && callBack !== onIsAlive) {
				hideSpinner();
				showDefaultSpinner = true;
				$('#main').xpull({ 'paused': false });			  
			  }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              isLoading = false;
              hideSpinner();
              showDefaultSpinner = true;
              try{resetXpull();}
			  catch(ex) {}
              showLoginPage();
              $("#lblErrorLogin").text(captions[currentLanguage].connection_error);
              $("#lblErrorLogin").show();
			  $('#main').xpull({ 'paused': false });					  
          }
      });

  } 
    

}

function resetXpull() {
    $('#main').data('plugin_xpull').reset();
    }

// XOR Encryption/Descruption functions

function encrypt(s,pw) {
                       var a=0;
                       var resultString="";
                       var textLen=s.length;
                          	var pwLen=pw.length;
                          	for (i=0;i<textLen;i++) {
                             a=parseInt(s.charCodeAt(i));
                             a=a^(pw.charCodeAt(i%pwLen));
                             a=a+"";
                            while (a.length<3)
                              a="0"+a;
                            	resultString+=a;
                          }
                         return resultString;
                      }

function decrypt(s,pw)  { 
  var resultString=""; 
	var a=0;
	var pwLen=pw.length;
	var textLen=s.length;
	var i=0;
	var resultHolder="";
	while(i<s.length-2)
	{
		 resultHolder=s.charAt(i)+s.charAt(i+1)+s.charAt(i+2);
		 if (s.charAt(i)=="0")
		{
			resultHolder=s.charAt(i+1)+s.charAt(i+2);
		}
		 if ((s.charAt(i)=="0")&&(s.charAt(i+1)=="0"))
		 {
			resultHolder=s.charAt(i+2);
		 }
		 a=parseInt(resultHolder);
		 a=a^(pw.charCodeAt(i/3%pwLen));
		 resultString+=String.fromCharCode(a);
		 i+=3;
	} 
	return resultString;
  }



  