/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
function registerPushwooshIOS() {
		
	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");

	//set push notifications handler
	document.addEventListener('push-notification',
		function(event)
		{
		   
		try {
			
				var title = event.notification.message;
		        var userData = event.notification.userdata;
			
				//dump custom data to the console if it exists
				if(typeof(userData) != "undefined") {
					console.warn('user data: ' + JSON.stringify(userData));
				}
				
			
				if(userData) {
					if(userData.route) {
						url = getRootURL() + userData.route;
						moduleMode = true;
						currentModuleRoute = url;
						refresh();
						}
				}
				else {
					alert(title);
				}
				
			pushNotification.setApplicationIconBadgeNumber(0);
			
			} catch(e) {
				alert(e.message);
			}
			pushNotification.setApplicationIconBadgeNumber(0);
			
		}
	);
	
		
	//initialize the plugin
	pushNotification.onDeviceReady({pw_appid:"233DE-9B03D"});
	
	//register for pushes
	pushNotification.registerDevice(
		function(status)
		{
			  
		  onPushwooshiOSInitialized(status);
			
		},
		function(status)
		{
			console.warn('failed to register : ' + JSON.stringify(status));
			alert(JSON.stringify(['failed to register ', status]));
		}
	);
	//reset badges on start
	pushNotification.setApplicationIconBadgeNumber(0);
}

function onPushwooshiOSInitialized(pushToken)
{
	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
	//retrieve the tags for the device
	pushNotification.getTags(
		function(tags) {
			console.warn('tags for the device: ' + JSON.stringify(tags));
		},
		function(error) {
			console.warn('get tags error: ' + JSON.stringify(error));
		}
	);

	//example how to get push token at a later time 
	pushNotification.getPushToken(
		function(token)
		{
			hwid = token;
			console.warn('push token device: ' + token);
		}
	);

	//example how to get Pushwoosh HWID to communicate with Pushwoosh API
	pushNotification.getPushwooshHWID(
		function (token) {
			console.warn('Pushwoosh HWID: ' + token);
		}
	);

	//start geo tracking.
	//pushNotification.startLocationTracking();
}
